# README #

This repo contains all code written by [me](http://holtappels.in)  for the course [*Text Mining & Collective Intelligence*](http://mashup2.science.uva.nl/marx/teaching/collective_intelligence/) at Amsterdam University College.

### What is this repository for? ###

* for me to document my progress
* for my fellow students to be able to compare their solutions against mine
* for anyone else working through either of the two books used in this course, [Natural Language Processing with Python](http://www.nltk.org/book/) and [Programming Collective Intelligence](http://shop.oreilly.com/product/9780596529321.do), who is looking for solutions to some of the exercises included in those books

### What is this repository NOT for? ###

* copying my code and pretending it is your own, e.g. for homework